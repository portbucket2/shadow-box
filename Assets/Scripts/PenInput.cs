using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenInput : MonoBehaviour
{
    [SerializeField] Camera mainCam;
    [SerializeField] float resolution;
    [SerializeField] float endGap;
    [SerializeField] MeshGenerator generator;
    [SerializeField] MeshGeneratorPro generatorPro;
    [SerializeField] List<Vector3> points;

    void Start()
    {
        points = new List<Vector3>();    
    }
    Vector3 previousePoint;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FirstPoint();
        }
        if (Input.GetMouseButton(0))
        {
            NewPoint();
        }
        if (Input.GetMouseButtonUp(0))
        {
            EndPoint();
        }
    }
    void FirstPoint()
    {
        points = new List<Vector3>();

        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.blue, 30f);
            points.Add(hit.point);
            previousePoint = Input.mousePosition;

            generatorPro.MeshQuadStart(hit.point);
        }

    }
    void NewPoint()
    {
        if (Vector3.Distance(Input.mousePosition, previousePoint)> resolution)
        {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray,out hit,100f))
            {
                Debug.DrawLine(ray.origin, hit.point, Color.blue);
                points.Add(hit.point);
                previousePoint = Input.mousePosition;

                //generator.CustomShape(points);
                //generatorPro.CreateMesh(points);
                generatorPro.CreateMeshQuads(hit.point);
            }            
        }
    }
    void EndPoint()
    {
        //generator.EndShape();
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.blue);
            points.Add(hit.point);
            previousePoint = Input.mousePosition;

            //generator.CustomShape(points);
            //generatorPro.CreateMesh(points);
            if (Vector3.Distance(points[0], hit.point)< endGap )
            {
                generatorPro.MeshQuadEndClosed();
            }
            else
            {
                generatorPro.MeshQuadEndOpen();
            }   
        }
    }
    private void OnDrawGizmos()
    {
        int max = points.Count;
        if (max <= 0)
            return;

        for (int i = 0; i < max-1; i++)
        {
            Gizmos.DrawLine(points[i], points[i + 1]);
            Gizmos.DrawSphere(points[i], 0.05f);
        }
    }
}
