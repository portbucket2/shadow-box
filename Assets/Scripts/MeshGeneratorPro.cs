using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;

public class MeshGeneratorPro : MonoBehaviour
{

    [SerializeField] float LineThickness;
    [SerializeField] Material mat;

    [Header("mesh operations: -------------------------")]
    [SerializeField] float extrude;
    [Header("Debug: -------------------------")]
    [SerializeField] DrawPoint previousPoint;
    [SerializeField] DrawPoint firstPoint;
    [SerializeField] Mesh mesh;
    [SerializeField] List<DrawPoint> drawPoints;
    [SerializeField] List<Vector3> pointsGlobal;
    [SerializeField] Vector3[] vertices;
    [SerializeField] List<Face> faces;

    [SerializeField] List<ProBuilderMesh> quads;

    Face faceUp;
    Face faceDown;
    Face mainFace;

    void Start()
    {
        faceUp = new Face(new int[] { 0, 1, 3, 0, 3, 2 });
        faceDown = new Face(new int[] { 0, 3, 1, 0, 2, 3 });
    }
    void CreateQuad(Vector3 pos1, Vector3 pos2, Vector3 pos3, Vector3 pos4,bool reversed)
    {
        mainFace = new Face();

        if (reversed) 
        {
            mainFace = faceDown;
        }
        else
        {
            mainFace = faceUp;
        }
        ProBuilderMesh quad = ProBuilderMesh.Create(
        new Vector3[] { pos1, pos2, pos3, pos4 }, new Face[] { mainFace });

        quad.SetMaterial(quad.faces, mat);
        quad.Refresh();
        quad.ToMesh();
        quads.Add(quad);
    }
    public void MeshQuadStart(Vector3 point)
    {
        ResetQuads();
        //Debug.Log("mesh quads start");
        previousPoint = new DrawPoint();
        previousPoint.ownPosition = point;
        previousPoint.clock = point + transform.right * LineThickness;
        previousPoint.antiClock = point - transform.right * LineThickness;
        firstPoint = new DrawPoint();
        firstPoint = previousPoint;
    }
    public void CreateMeshQuads(Vector3 point)
    {
        Vector3 dr1 = previousPoint.ownPosition;

        Vector3 dir = dr1 - point;
        //Debug.DrawLine(dr1, point , Color.cyan, 10f);
        dir.z = 0;
        Vector3 clock;
        Vector3 anticlock;

        clock = new Vector3(-dir.y, dir.x, 0f);
        anticlock = new Vector3(dir.y, -dir.x, 0f); 
        clock.Normalize();
        anticlock.Normalize();
        //Debug.DrawRay(point, clock, Color.green, 20f);
        //Debug.DrawRay(point, anticlock, Color.blue, 20f);
        Debug.DrawLine(point, point + clock * LineThickness, Color.green, 30f);
        Debug.DrawLine(point, point + anticlock * LineThickness, Color.blue, 30f);

        CreateQuad(previousPoint.clock, previousPoint.antiClock, point + clock * LineThickness, point + anticlock * LineThickness, false);
        CreateQuad(previousPoint.clock, previousPoint.antiClock, point + clock * LineThickness, point + anticlock * LineThickness, true);

        previousPoint = new DrawPoint();
        previousPoint.ownPosition = point;
        previousPoint.clock = point + clock * LineThickness;
        previousPoint.antiClock = point + anticlock * LineThickness;
        //Debug.Log("create mesh quads");

       
    }
    IEnumerable<ProBuilderMesh> allQuads;
    //IEnumerable<Edge> allEdges;
    public void MeshQuadEndClosed()
    {
        CreateQuad(previousPoint.clock, previousPoint.antiClock, firstPoint.clock, firstPoint.antiClock, false);
        CreateQuad(previousPoint.clock, previousPoint.antiClock, firstPoint.clock, firstPoint.antiClock, true);
        allQuads = quads;
        List<ProBuilderMesh> gg = new List<ProBuilderMesh>();
        gg = CombineMeshes.Combine(allQuads);
        ResetQuads();
        StartCoroutine(Extrution(gg));

    }
    IEnumerator Extrution(List<ProBuilderMesh> _mesh)
    {
        ProBuilderMesh newMesh = _mesh[0];
        //newMesh.ToMesh();
        //newMesh.Refresh();
        yield return null;
        Face[] newFaces;
        newFaces = ExtrudeElements.Extrude(_mesh[0], _mesh[0].faces, ExtrudeMethod.FaceNormal, extrude);
        yield return null;
        // combine
        ProBuilderMesh extrudedMesh = _mesh[0];
        _mesh = new List<ProBuilderMesh>();
        _mesh.Add(extrudedMesh);
        _mesh.Add(newMesh);
        List<ProBuilderMesh> ExtrudedCombinedMesh = new List<ProBuilderMesh>();
        ExtrudedCombinedMesh = CombineMeshes.Combine(_mesh);
        yield return null;
        ExtrudedCombinedMesh[0].ToMesh();
        ExtrudedCombinedMesh[0].Refresh();

        ExtrudedCombinedMesh[0].transform.position += Vector3.forward * extrude;
    }
    public void MeshQuadEndOpen()
    {
        //CreateQuad(previousPoint.clock, previousPoint.antiClock, firstPoint.clock, firstPoint.antiClock);
        allQuads = quads;
        List<ProBuilderMesh> gg = new List<ProBuilderMesh>();
        gg = CombineMeshes.Combine(allQuads);
        ResetQuads();
        //Face[] newFaces;
        //newFaces = ExtrudeElements.Extrude(gg[0], gg[0].faces, ExtrudeMethod.FaceNormal, extrude);
    }
    public void CreateMesh(List<Vector3> points)
    {
        drawPoints = new List<DrawPoint>();
        pointsGlobal = new List<Vector3>();
        pointsGlobal = points;
        int max = points.Count;
        for (int i = 0; i < max; i++)
        {
            Thickness(points[i]);
        }
        int j = 0;
        int vertCount = drawPoints.Count * 2;
        vertices = new Vector3[vertCount];
        for ( int i = 0; i < vertCount; i+=2)
        {
            vertices[i] = drawPoints[j].clock;
            vertices[i + 1] = drawPoints[j].antiClock;
            j++;
        }
        faces = new List<Face>();

        int drawTrisLoops = (drawPoints.Count - 1) * 2;
        //int vert = 0;
        for (int x = 0; x < drawTrisLoops; x += 2)
        {
            int aa = x + 0;
            int bb = x + 1;
            int cc = x + 3;
            int dd = x + 0;
            int ee = x + 3;
            int ff = x + 2;
            Face face1 = new Face(new int[] { aa, bb, cc });
            Face face2 = new Face(new int[] { dd, ee, ff });

            //triangles[vert + 0] = aa;
            //triangles[vert + 1] = bb;
            //triangles[vert + 2] = cc;
            //triangles[vert + 3] = dd;
            //triangles[vert + 4] = ee;
            //triangles[vert + 5] = ff;
            //vert += 6;
        }

        ProBuilderMesh line = ProBuilderMesh.Create(vertices, faces);
        line.SetMaterial(line.faces, mat);
        line.Refresh();
        line.ToMesh();

    }
    void Thickness(Vector3 dr2)
    {
        Vector3 dr1 = Vector3.zero;
        if (drawPoints.Count <= 0)
        {
            dr1 = pointsGlobal[0];
        }
        else
        {
            dr1 = previousPoint.ownPosition;
        }
        Vector3 dir = dr1 - dr2;
        dir.z = 0;
        Vector3 clock;
        Vector3 anticlock;

        clock = new Vector3(dir.y, -dir.x, 0f);
        anticlock = new Vector3(-dir.y, dir.x, 0f);

        //if (drawPoints.Count <= 0)
        //    AddDrawPoint(dr1, clock, anticlock);


        AddDrawPoint(dr2, clock, anticlock);

        previousPoint = new DrawPoint();
        previousPoint.ownPosition = dr2;
        previousPoint.clock = clock;
        previousPoint.antiClock = anticlock;

    }
    void AddDrawPoint(Vector3 pt, Vector3 clk, Vector3 antiClk)
    {
        DrawPoint ddd = new DrawPoint();
        antiClk.Normalize();
        clk.Normalize();
        ddd.ownPosition = pt;
        ddd.clock = pt + antiClk * LineThickness;
        ddd.antiClock = pt + clk * LineThickness;

        drawPoints.Add(ddd);

        Debug.DrawLine(ddd.ownPosition, ddd.ownPosition + clk * LineThickness, Color.green, 30f);
        Debug.DrawLine(ddd.ownPosition, ddd.ownPosition + antiClk * LineThickness, Color.blue, 30f);
    }
    void ResetQuads()
    {
        if (quads.Count <= 0)
            return;

        int max = quads.Count;
        for (int i = 0; i < max; i++)
        {
            Destroy(quads[i].gameObject);
        }
        quads = new List<ProBuilderMesh>();
        previousPoint = new DrawPoint();
        firstPoint = new DrawPoint();
        
    }
}
