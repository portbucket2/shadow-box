using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(MeshFilter))]
public class MeshGenerator : MonoBehaviour
{

    
    
    int[] triangles;
    [SerializeField] Button createButton;

    [SerializeField] int xSize; 
    [SerializeField] int ySize;
    [SerializeField] float LineThickness;
    [Space(10)]
    [Header("Debug: -------------------------")]
    [SerializeField] DrawPoint previousPoint;
    [SerializeField] Mesh mesh;
    [SerializeField] List<DrawPoint> drawPoints;
    [SerializeField] List<Vector3> pointsGlobal;
    [SerializeField] Vector3[] vertices;

    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        createButton.onClick.AddListener(delegate {
            CreateShape();
            UpdateMesh();
        });
        
    }


    void CreateShape()
    {
        vertices = new Vector3[(xSize + 1) * (ySize + 1)];


        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                vertices[i] = new Vector3(x, y, 0);
                i++;
            }
        }
        triangles = new int[xSize * ySize * 6];

        int vert = 0;
        int tris = 0;


        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }
    }
    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
    }

    public void CustomShape(List<Vector3> points)
    {
        //CircularShape(points);
        LineShape(points);

    }
    public void EndShape()
    {
        if (Vector3.Distance(drawPoints[0].ownPosition, drawPoints[drawPoints.Count - 1].ownPosition) < 3)
        {
            Debug.Log("end of line!!");
            //LineShapeEnd();
            pointsGlobal.Add(pointsGlobal[0]);
            LineShape(pointsGlobal);
        }
    }
    void LineShape(List<Vector3> points)
    {
        pointsGlobal = new List<Vector3>();
        pointsGlobal = points;

        drawPoints = new List<DrawPoint>();

        //Thickness(points[1]);
        int pp = points.Count;
        for (int i = 1; i < pp; i++)
        {
            Thickness(points[i]);
        }
        
        // creating mesh

        int max = drawPoints.Count;

        vertices = new Vector3[max * 2];
        int vv = 0;
        for (int i = 0; i < max; i++)
        {
            vertices[vv] = drawPoints[i].clock;
            vertices[vv + 1] = drawPoints[i].antiClock;
            vv += 2;
        }
        int trisCount = (points.Count - 1) * 6;
        triangles = new int[trisCount];
        Debug.Log("tris count: " + trisCount);
        int drawTrisLoops = (drawPoints.Count - 1) * 2;
        int vert = 0;
        for (int x = 0; x < drawTrisLoops; x += 2)
        {
            int aa = x + 0;
            int bb = x + 1;
            int cc = x + 3;
            int dd = x + 0;
            int ee = x + 3;
            int ff = x + 2;

            int aaVert = 0;
            int bbVert = vert + 1;
            int ccVert = vert + 2;

            //Debug.Log("aa: " + aa + " / vert: " + aaVert);
            //Debug.Log("bb: " + bb + " / vert: " + bbVert);
            //Debug.Log("cc: " + cc + " / vert: " + ccVert);
            triangles[vert + 0] = aa;
            triangles[vert + 1] = bb;
            triangles[vert + 2] = cc;
            triangles[vert + 3] = dd;
            triangles[vert + 4] = ee;
            triangles[vert + 5] = ff;
            vert += 6;
        }
        
        UpdateMesh();

    }
    void LineShapeEnd()
    {

        Thickness(pointsGlobal[0]);

        // creating mesh

        int preVert = vertices.Length;
        vertices = new Vector3[preVert + 2];

        vertices[preVert ] = drawPoints[drawPoints.Count - 1].clock;
        vertices[preVert + 1] = drawPoints[drawPoints.Count - 1].antiClock;


        int drawTrisLoops = (drawPoints.Count - 1) * 2;
        int x = drawTrisLoops - 3;
        int aa = x + 0;
        int bb = x + 1;
        int cc = x + 3;
        int dd = x + 0;
        int ee = x + 3;
        int ff = x + 2;

        int tris = triangles.Length;

        triangles = new int[triangles.Length + 6];

        triangles[tris + 0] = aa;
        triangles[tris + 1] = bb;
        triangles[tris + 2] = cc;
        triangles[tris + 3] = dd;
        triangles[tris + 4] = ee;
        triangles[tris + 5] = ff;

        UpdateMesh();
    }
    void Thickness( Vector3 dr2)
    {
        Vector3 dr1 = Vector3.zero;
        if (drawPoints.Count <= 0)
        {
            dr1 = pointsGlobal[0];
        }
        else
        {
            dr1 = previousPoint.ownPosition;
        }
        Vector3 dir = dr1 - dr2;
        dir.z = 0;
        Vector3 clock;
        Vector3 anticlock;

        clock = new Vector3(dir.y,-dir.x, 0f);
        anticlock = new Vector3(-dir.y,dir.x, 0f);

        if (drawPoints.Count <= 0)
            AddDrawPoint(dr1,clock, anticlock);


        AddDrawPoint(dr2,clock, anticlock);

        previousPoint = new DrawPoint();
        previousPoint.ownPosition = dr2;
        previousPoint.clock = clock;
        previousPoint.antiClock = anticlock;

    }
    void AddDrawPoint(Vector3 pt, Vector3 clk, Vector3 antiClk)
    {
        DrawPoint ddd = new DrawPoint();
        antiClk.Normalize();
        clk.Normalize();
        ddd.ownPosition = pt;
        ddd.clock = pt + antiClk * LineThickness;
        ddd.antiClock = pt + clk * LineThickness;

        drawPoints.Add(ddd);

        Debug.DrawLine(ddd.ownPosition, ddd.ownPosition + clk * LineThickness, Color.green, 30f);
        Debug.DrawLine(ddd.ownPosition, ddd.ownPosition + antiClk * LineThickness, Color.blue, 30f);
    }
    void CircularShape(List<Vector3> _points)
    {
        int max = _points.Count;

        vertices = new Vector3[max];
        for (int i = 0; i < max; i++)
        {
            vertices[i] = _points[i];
        }
        int trisCount = (max - 2) * 3;
        triangles = new int[trisCount];
        Debug.Log("tris count: " + trisCount);
        int vert = 0;
        for (int x = 0; x < trisCount; x += 3)
        {
            int aa = x + 0;
            int bb = x + 1;
            int cc = x + 2;

            int aaVert = 0;
            int bbVert = vert + 1;
            int ccVert = vert + 2;

            Debug.Log("aa: " + aa + " / vert: " + aaVert);
            Debug.Log("bb: " + bb + " / vert: " + bbVert);
            Debug.Log("cc: " + cc + " / vert: " + ccVert);
            triangles[aa] = aaVert;
            triangles[bb] = bbVert;
            triangles[cc] = ccVert;
            vert++;
        }
        UpdateMesh();
    }

    private void OnDrawGizmos()
    {
        if (vertices == null)
            return;

        for (int i = 0; i < vertices.Length; i++)
        {
            Gizmos.DrawSphere(vertices[i], 0.1f);
        }
    }
}
[System.Serializable]
public struct DrawPoint
{
    public Vector3 ownPosition;
    public Vector3 clock;
    public Vector3 antiClock;
}
